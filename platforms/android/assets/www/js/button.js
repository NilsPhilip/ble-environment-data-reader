
/* global index.html : refreshBtn, disconnectBtn, connectBtn */

/* global index.css : greyout */

var buttons = {
    
    initialize : function() {
        buttons.refreshBtn = new Button("refreshBtn", "scan for devices");
        buttons.disconnectBtn = new Button("disconnectBtn", "disconnect");
        buttons.connectBtn = new Button("connectBtn", "connect");
        
        buttons.addMeasurementBtn = new Button("addMeasurementBtn", "connect");
        buttons.removeMeasurementBtn = new Button("removeMeasurementBtn", "connect");
        buttons.selectAllMeasurementBtn = new Button("selectAllMeasurementBtn", "connect");
        
        buttons.createDbBtn = new Button("createDbBtn", "connect");
        buttons.postQueryBtn = new Button("postQueryBtn", "connect");
        buttons.getQueryBtn = new Button("getQueryBtn", "connect");
        
        buttons.notTempBtn = new ToggleButton("notTempBtn", "start temperature notification", "temperature notification", "stop temperature notification");
        buttons.notPressBtn = new ToggleButton("notPressBtn", "start pressure notification", "pressure notification", "stop pressure notification");
        buttons.notHumBtn = new ToggleButton("notHumBtn", "start humidity notification", "humidity notification", "stop humidity notification");
        buttons.notDustBtn = new ToggleButton("notDustBtn", "start dust notification", "dust notification", "stop dust notification");
        buttons.notCoBtn = new ToggleButton("notCoBtn", "start co notification", "co notification", "stop co notification");
        buttons.notNo2Btn = new ToggleButton("notNo2Btn", "start no2 notification", "no2 notification", "stop no2 notification");
        buttons.notNh3Btn = new ToggleButton("notNh3Btn", "start nh2 notification", "nh2 notification", "stop nh2 notification");

        buttons.deactivateNotificationButtons();
    },
    
    activateNotificationButtons : function() {
        buttons.notTempBtn.activate(
            {
                type : "click",
                func : function()
                    {
                        connection.startNotification(sensorListener.receiveTemperature, sensorListener.receiveTempFailure, "temperature", ENV_SERVICE, ENV_TEMP);
                        buttons.notTempBtn.highlight();
                    }
            },
            {
                type : "click",
                func : function()
                    {
                        connection.stopNotification("temperature", ENV_SERVICE, ENV_TEMP);
                        buttons.notTempBtn.activate();
                    }
            }
        );
        buttons.notPressBtn.activate(
            {
                type : "click",
                func : function()
                    {
                        connection.startNotification(sensorListener.receivePressure, sensorListener.receivePressFailure, "pressure", ENV_SERVICE, ENV_PRESS);
                        buttons.notPressBtn.highlight();
                    }
            },
            {
                type : "click",
                func : function()
                    {
                        connection.stopNotification("pressure", ENV_SERVICE, ENV_PRESS);
                        buttons.notPressBtn.activate();
                    }
            }
        );
        buttons.notHumBtn.activate(
            {
                type : "click",
                func : function()
                    {
                        connection.startNotification(sensorListener.receiveHumidity, sensorListener.receiveHumFailure, "humidity", ENV_SERVICE, ENV_HUM);
                        buttons.notHumBtn.highlight();
                    }
            },
            {
                type : "click",
                func : function()
                    {
                        connection.stopNotification("humidity", ENV_SERVICE, ENV_HUM);
                        buttons.notHumBtn.activate();
                    }
            }
        );
        buttons.notDustBtn.activate(
            {
                type : "click",
                func : function()
                    {
                        connection.startNotification(sensorListener.receiveDust, sensorListener.receiveDustFailure, "dust", DUST_SERVICE, DUST_RAW);
                        buttons.notDustBtn.highlight();
                    }
            },
            {
                type : "click",
                func : function()
                    {
                        connection.stopNotification("dust", DUST_SERVICE, DUST_RAW);
                        buttons.notDustBtn.activate();
                    }
            }
        );
        buttons.notCoBtn.activate(
            {
                type : "click",
                func : function()
                    {
                        connection.startNotification(sensorListener.receiveCoRaw, sensorListener.receiveCoFailure, "co raw", GAS_SERVICE, GAS_CO_RAW);
                        connection.startNotification(sensorListener.receiveCoCalib, sensorListener.receiveCoFailure, "co calib", GAS_SERVICE, GAS_CO_CALIB);
                        buttons.notCoBtn.highlight();
                    }
            },
            {
                type : "click",
                func : function()
                    {
                        connection.stopNotification("co raw", GAS_SERVICE, GAS_CO_RAW);
                        connection.stopNotification("co calib", GAS_SERVICE, GAS_CO_CALIB);
                        buttons.notCoBtn.activate();
                    }
            }
        );
        buttons.notNo2Btn.activate(
            {
                type : "click",
                func : function()
                    {
                        connection.startNotification(sensorListener.receiveNo2Calib, sensorListener.receiveNo2Failure, "no2 calib", GAS_SERVICE, GAS_NO2_CALIB);
                        connection.startNotification(sensorListener.receiveNo2Raw, sensorListener.receiveNo2Failure, "no2 raw", GAS_SERVICE, GAS_NO2_RAW);
                        buttons.notNo2Btn.highlight();
                    }
            },
            {
                type : "click",
                func : function()
                    {
                        connection.stopNotification("no2 calib", GAS_SERVICE, GAS_NO2_CALIB);
                        connection.stopNotification("no2 raw", GAS_SERVICE, GAS_NO2_RAW);
                        buttons.notNo2Btn.activate();
                    }
            }
        );
        buttons.notNh3Btn.activate(
            {
                type : "click",
                func : function()
                    {
                        connection.startNotification(sensorListener.receiveNh3Calib, sensorListener.receiveNh3Failure, "nh3 calib", GAS_SERVICE, GAS_NH3_CALIB);
                        connection.startNotification(sensorListener.receiveNh3Raw, sensorListener.receiveNh3Failure, "nh3 raw", GAS_SERVICE, GAS_NH3_RAW);
                        buttons.notNh3Btn.highlight();
                    }
            },
            {
                type : "click",
                func : function()
                    {
                        connection.stopNotification("nh3 calib", GAS_SERVICE, GAS_NH3_CALIB);
                        connection.stopNotification("nh3 raw", GAS_SERVICE, GAS_NH3_RAW);
                        buttons.notNh3Btn.activate();
                    }
            }
        );
    },
    
    deactivateNotificationButtons : function() {
        buttons.notTempBtn.deactivate();
        buttons.notPressBtn.deactivate();
        buttons.notHumBtn.deactivate();
        buttons.notDustBtn.deactivate();
        buttons.notCoBtn.deactivate();
        buttons.notNo2Btn.deactivate();
        buttons.notNh3Btn.deactivate();
    },
    
    loadMeasurementButtons : function() {
        buttons.selectAllMeasurementBtn.activate(
            {
                type : "click",
                func : function()
                    {
                        if(measurementlist.isEmpty()) {
                            displays.event.starting("no measurements");
                        } else if(measurementlist.allSelected()) {
                            measurementlist.deselectAll();
                        } else {
                            measurementlist.selectAll();
                        }
                    }
            }
        );
        buttons.addMeasurementBtn.activate(
            {
                type : "click",
                func : function()
                    {
                        if(app.isConnected()) {
                            // add measurement to list
                            measurementlist.add(sensorListener.getMeasurement());
                            displays.event.completion("took measurement");
                        } else {
                            displays.event.starting("connect first");
                        }
                    }
            }
        );
        buttons.removeMeasurementBtn.activate(
            {
                type : "click",
                func : function()
                    {
                        if(measurementlist.hasSelected()) {
                            measurementlist.remove();
                            displays.event.completion("selection removed");
                        } else {
                            displays.event.starting("selection empty");
                        }
                    }
            }
        );
    },
    
    loadTsdbButtons : function() {
        buttons.createDbBtn.activate(
            {
                type : "click",
                func : function()
                    {
                        tsdb.setId($("#tsdb-form-id-value").val());
                        tsdb.setName($('#tsdb-text').val());
                        tsdb.setPassword($('#tsdb-pw').val());
                        tsdb.createDatabase();
                    }
            }
        );
        buttons.postQueryBtn.activate(
            {
                type : "click",
                func : function()
                    {
                        tsdb.setId($("#tsdb-form-id-value").val());
                        tsdb.setName($('#tsdb-text').val());
                        tsdb.setPassword($('#tsdb-pw').val());
                        tsdb.writeData();
                    }
            }
        );
        buttons.getQueryBtn.activate(
            {
                type : "click",
                func : function()
                    {
                        tsdb.setId($("#tsdb-form-id-value").val());
                        tsdb.setName($('#tsdb-text').val());
                        tsdb.setPassword($('#tsdb-pw').val());
                        tsdb.readDataAndDisplay();
                    }
            }
        );
    },
    
    loadNetworkButtons : function() {
        buttons.refreshBtn.activate(
            {
                type : "click",
                func : function() { app.react("scan"); }
            }
        );
        buttons.connectBtn.deactivate(
            {
                type : "click",
                func : function() { app.react("connect"); }
            }
        );
        buttons.disconnectBtn.deactivate(
            {
                type : "click",
                func : function() { app.react("disconnect"); }
            }
        );
    }
}

/**
 * TODO: more than one listener
 *
 * btnId : String
 * activeText : String
 * func : function
 */
function Button(btnId, activeText) {
	this.id = btnId;
	this.element = document.getElementById(this.id);
    this.active = activeText;
	this.hidden = activeText;
    this.listener = [];
    
	this.activate = function(newListener) {
        if(arguments.length == 1) {
            this.register(newListener);
        } else {
            this.register(this.listener);
        }
		this.show();
	};
    
    this.deactivate = function(newListener) {
        if(arguments.length == 1) {
            this.register(newListener);
        }
        this.deregister();
		this.hide();
	};
    
    this.deactivateHighlight = function(newListener) {
        if(arguments.length == 1) {
            this.register(newListener);
        }
        this.deregister();
        $("#" + this.id).removeClass("greyout");
		$("#" + this.id).addClass("highlight");
	};
    
	/**
	 * add a listener.
     *
     * example format: var listener = {type : "click", func : function() { }};
	 */
	this.register = function(newListener) {
        this.deregister();
        this.listener = newListener;
		this.element.addEventListener(
                newListener["type"],
                newListener["func"]
		);
	};
	/**
	 * remove a listener.
	 */
	this.deregister = function() {
		this.element.removeEventListener(
                this.listener["type"],
                this.listener["func"]
		);
	};
    
    this.show = function() {
        $("#" + this.id).removeClass("highlight");
		$("#" + this.id).removeClass("greyout");
	};
    
    this.hide = function() {
        $("#" + this.id).removeClass("highlight");
		$("#" + this.id).addClass("greyout");
	};

	this.text = function() {
		return this.active;
	};
    
	this.getHiddenText = function() {
		return this.hide;
	};

    /**
	 * change the text of the button.
     * animation fadeOut and fadeIn
	 * 
	 * newName : String
	 */
	this.changeName = function(newName) {
		// fadeout out animation to let user know something has changed
		$("#" + this.id).fadeOut();
		this.setName(newName);
		$("#" + this.id).fadeIn();
	};
    
    /**
	 * set the text of the button.
	 * 
	 * newName : String
	 */
	this.setName = function(newName) {
		$("#" + this.id).html(newName);
	};
    
    this.setHiddenText = function(hiddenText) {
		this.hidden = hiddenText;
	};
    
    this.setActiveText = function(activeText) {
		this.active = activeText;
	};
}

function ToggleButton(id, activeText, inactiveText, highlightText) {
	this.id = id;
	this.element = document.getElementById(this.id);
    this.active = activeText;
	this.inactive = inactiveText;
    this.highlighted = highlightText;
    this.isActive = true;
    this.activeListener = [];
    this.highlightListener = [];
    
    this.highlight = function(activeListener, highlightListener) {
        if(arguments.length == 2) {
            this.activeListener = activeListener;
            this.registerHighlightListener(highlightListener);
        } else {
            this.registerHighlightListener(this.highlightListener);
        }
		this.highlightStyle();
	};
    
	this.activate = function(activeListener, highlightListener) {
        if(arguments.length == 2) {
            this.highlightListener = highlightListener;
            this.registerActiveListener(activeListener);
        } else {
            this.registerActiveListener(this.activeListener);
        }
		this.activeStyle();
	};
    
    this.deactivate = function(activeListener, highlightListener) {
        if(arguments.length == 2) {
            this.highlightListener = highlightListener;
            this.activeListener = activeListener;
        }
        this.deregister();
		this.hiddenStyle();
	};

	this.registerActiveListener = function(newListener) {
        this.deregister();
        this.activeListener = newListener;
		this.element.addEventListener(
                newListener["type"],
                newListener["func"]
		);
	};
    
    this.registerHighlightListener = function(newListener) {
        this.deregister();
        this.highlightListener = newListener;
		this.element.addEventListener(
                newListener["type"],
                newListener["func"]
		);
	};

	this.deregister = function() {
		this.element.removeEventListener(
                this.activeListener["type"],
                this.activeListener["func"]
		);
        this.element.removeEventListener(
                this.highlightListener["type"],
                this.highlightListener["func"]
		);
	};
    
    this.activeStyle = function() {
        $("#" + this.id).removeClass("green");
		$("#" + this.id).removeClass("greyout");
        this.setName(this.active);
	};
    
    this.highlightStyle = function() {
        $("#" + this.id).removeClass("greyout");
		$("#" + this.id).addClass("green");
        this.setName(this.highlighted);
	};
    
    this.hiddenStyle = function() {
        $("#" + this.id).removeClass("green");
		$("#" + this.id).addClass("greyout");
        this.setName(this.inactive);
	};

	this.text = function() {
		return this.active;
	};
    
	this.getHiddenText = function() {
		return this.hide;
	};

	this.changeName = function(newName) {
		// fadeout out animation to let user know something has changed
		$("#" + this.id).fadeOut();
		this.setName(newName);
		$("#" + this.id).fadeIn();
	};

	this.setName = function(newName) {
		$("#" + this.id).html(newName);
	};
    
    this.setInactiveText = function(text) {
		this.inactive = text;
	};
    
    this.setActiveText = function(text) {
		this.active = text;
	};
    
    this.setHighlightText = function(text) {
		this.highlight = text;
	};
}