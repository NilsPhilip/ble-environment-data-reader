
/* global index.html : state, event */

var displays = {
    
    state : [],

    event : [],
    
    initialize : function() {

        displays.state =  {
            display : [],
            
            /**
             * events.
             *
             * 1. call set(...) to set the attributes (data)
             * 2. call show(...) to show the attributes (output)
             */
            
            defaultEvent : function() {
                displays.state.startEvent(displays.state.display.defaultName);
            },
            
            showEvent : function(name, type) {
                displays.state.display.set(name, type);
                displays.state.display.show();
            },
            
            startEvent : function(name) {
                displays.state.display.set(name, displays.state.display.defaultStartType);
                displays.state.display.show();
            },
            
            completeEvent : function(name) {
                displays.state.display.set(name, displays.state.display.defaultCompleteType);
                displays.state.display.show();
            },
            
            failedEvent : function(name) {
                displays.state.display.set(name, displays.state.display.defaultFailedType);
                displays.state.display.show();
            }
        };
        
        var hideEventDisplay = function() {
            $("#" + displays.event.display.htmlElement).css('visibility', 'hidden');
        };
        var showEventDisplay = function() {
            $("#" + displays.event.display.htmlElement).css('visibility', 'visible');
        };
        
        displays.event = {
            
            display : [],
            
            /**
             * show an event for a limited time.
             */
            
            completion : function(name) {
                displays.event.show("&#10004; " + name, displays.event.display.defaultCompleteType);
            },
            
            starting : function(name) {
                displays.event.show("&#8505; " + name, displays.event.display.defaultStartType);
            },
            
            failure : function(name) {
                displays.event.show("&#9940; " + name, displays.event.display.defaultFailedType);
            },
            
            show : function(name, type) {
                displays.event.display.set(
                    name,
                    type
                );
                displays.event.display.showTimeout(
                    showEventDisplay,
                    hideEventDisplay
                );
            },
        };
        displays.state.display = new Display("state");
        displays.event.display = new Display("event");
    }
}

function Display(htmlName) {
    this.defaultName = "waiting for device";
    
    this.htmlElement = htmlName;
    
    this.defaultStartType = "loading";
    
    this.defaultCompleteType = "finished";
    
    this.defaultFailedType = "failed";
    
    this.startName = [];
    
    this.stopName = [];
    
    this.startType = [];
    
    this.stopType = [];
        
    this.timeoutId = [];
    
    /**
     * stopName : optional
     * stopType : optional
     */
    this.set = function(startName, startType, stopName, stopType) {
        clearTimeout(this.timeoutId);
        this.startName = startName;
        this.startType = startType;
        if (arguments.length > 2) {
            this.stopName = stopName;
            this.stopType = stopType;
        }
    };
    
    //this.reset = function() {
    //    this.set(false, false, false, false);
    //};
    
    /**
     * name : optional
     * type : optional
     */
    this.show = function(name, type) {
        if (arguments.length == 0) {
            name = this.startName;
            type = this.startType;
        }
        var htmlElement = document.getElementById(this.htmlElement);
        htmlElement.innerHTML = name;
        htmlElement.className = type;
        view.log("display received " + this.htmlElement + ": " + name);
    };
    
    this.showTimeout = function(before, handler) {
        before();
        this.show();
        this.timeoutId = setTimeout(handler, 5000);
    };
}