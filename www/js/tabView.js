

var tabView = {
    
    tabId : "#content",
    
    ready : {
        connection : false,
        data: false,
        tsdb : false    
    },
    
    initialize : function() {
        $(tabView.tabId).tabs();
        
        tabView.connection = new TabView("connection", tabView.initConnectionView, tabView.onConnectionView, function() { });
        tabView.data = new TabView("data", tabView.initDataView, tabView.onDataView, tabView.offDataView);
        tabView.tsdb = new TabView("tsdb", tabView.initTsdbView, tabView.onTsdbView, tabView.offTsdbView);
        
        // initConnectionView does not get called on the default tabView
        tabView.current = tabView.connection; //default    
        tabView.load("connection view init", tabView.initConnectionView);
        tabView.ready["connection"] = true;
        tabView.load("connection view call", tabView.onConnectionView);
        
        $(tabView.tabId).tabs(
            {
                activate: function(event, ui) {
                   // gets called when a tab is selected
                    var name = ui.newPanel.attr('id').replace("fragment-", "");
                    tabView.current = tabView[name];
                    tabView.activateView(name);
                    var old = ui.oldPanel.attr('id').replace("fragment-", "");
                    tabView[old].off();
                }
            }
        );
        tabView.load("measurement list", measurementlist.initialize);
        tabView.load("tsdb list", tsdblist.initialize);
        tabView.load("sensor listener", sensorListener.initialize);
    },
    
    activateView : function(name) {
        if(!tabView.ready[name]) {
            tabView.load(name + " view init", tabView.current.init);
            tabView.ready[name] = true;
        }
        tabView.load(name + " view active", tabView.current.on);
    },
    
    initConnectionView : function() {
        tabView.load("displays", displays.initialize);
        tabView.load("button objects", buttons.initialize);    
        tabView.load("network buttons", buttons.loadNetworkButtons);  
        tabView.load("tsdb buttons", buttons.loadTsdbButtons);        
        tabView.load("measurement buttons", buttons.loadMeasurementButtons);
        tabView.load("states", app.initializeStates);
        tabView.load("device list", devicelist.initialize);
    },
    
    initDataView : function() {
        $("#graphContainer").children().css('display', 'none');
        $("#graph-text").css('display', 'block');
        // make-do object for jquery val function
        // to return a value on first call.
        tabView.selection = {
            val : function() { return "none"; }
        };
        $(document).on(
            'change',
            '#graphSelection',
            function() {
                tabView.selection = $(this);
                tabView.onDataView();
            }
        );
    },
    
    initTsdbView : function() {
        
    },
    
    onConnectionView : function() {
        var newHeight = Math.floor($(window).height() - app.contentOffsetTop - $("#nav").outerHeight() - 20);
        devicelist.height(newHeight);
    },
    
    onDataView : function() {
        $("#graphContainer").children().css('display', 'none');
        
        switch (tabView.selection.val())
        {
            case "none":
                $("#graph-text").css('display', 'block');
                
                break;
            case "notification":
                $("#notification-display").css('display', 'block');
                break;
            case "measurements":
                $("#measurement-display").css('display', 'block');
                var newHeight = Math.floor($(window).height() - app.contentOffsetTop - $("#nav-measurement").outerHeight() - 80);
                $("#measurement-display").height(newHeight);
                $("#measurement-list-body").height(newHeight);
                break;
            case "simple":
                $("#deviceInfo").css('display', 'block');
                simple.on();
                break;
            case "linechart":
                $("#lineChart").css('display', 'block');
                var newHeight = Math.floor($(window).height() - app.contentOffsetTop - $("#choice").outerHeight() - 20);
                $("#graphContainer").height(newHeight);
                $("#lineChart").height(newHeight);
                $("#myChart").height(newHeight);
                $("#myChart").width("100%");
                tabView.load("graph", graph.load);
                graph.on();
                break;
            default:
                displays.event.failure("unknown selection view error");
                break;
        }
    },
    
    offDataView : function() {
        simple.off();
        graph.off();
    },
    
    onTsdbView : function() {
        var newHeight = Math.floor($(window).height() - app.contentOffsetTop - $("#nav-tsdb").outerHeight() - $("#tsdb-form-id").outerHeight() - $("#tsdb-form").outerHeight() - 20);
        $("#tsdb-display").height(newHeight);
        $("#tsdb-list-body").height(newHeight);
        
        if(measurementlist.hasSelected()) {
            var copy = [];
            var selected = measurementlist.getSelected();
            for(var i = 0; i < selected.length; i++) {
                copy[i] = sensorListener.copy(selected[i]);
            }
            tsdblist.addAll(copy);
        } else {
            tsdblist.clear();
        }
    },
    
    offTsdbView : function() {
        tsdblist.clear();
    },
    
    load : function(name, func) {
        view.log("load " + name + "...");
        func();
        view.log("loaded " + name);
    }
}

function TabView(name, init, on, off) {
	this.name = name;
	this.init = init;
    this.on = on;
	this.off = off;
}

