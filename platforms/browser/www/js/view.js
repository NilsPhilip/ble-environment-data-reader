
/* global index.html : ul logList */

var view = {
    showLog : false,
    
    logId : "logList",
    
    log : function(name) {
        console.log('received event: ' + name);
        
        if (this.showLog) {
            var myLog = document.getElementById(this.logId);
            var logItem = document.createElement('li');
            logItem.innerHTML = new Date().timeNow() + " : " +  name + "<br/>";
            myLog.appendChild(logItem);
        }
    }
}

/* http://stackoverflow.com/questions/10211145 */
Date.prototype.timeNow = function () {
     return ((this.getHours() < 10)?"0":"") + this.getHours() +":"+ ((this.getMinutes() < 10)?"0":"") + this.getMinutes() +":"+ ((this.getSeconds() < 10)?"0":"") + this.getSeconds();
}
