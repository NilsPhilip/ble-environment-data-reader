
/* example object of 'startScan' (android)
 *    {
 *       "name": "TI SensorTag",
 *       "id": "BD922605-1B07-4D55-8D09-B66653E51BBA",
 *       "rssi": -79,
 *       "advertising": // ArrayBuffer or map
 *    }
 */
 
var scanner = {
    
    scanning : false,
    
    scanTimeout : 10000,
    
    timeoutId : [],
    
    refreshDeviceList : function() {
        ble.isEnabled(
            scanner.scan,
            function() {
                view.log("scanner: failed bluetooth disabled");
                app.react("scan failed: bluetooth disabled");
            }
        );
    },
    
    scan : function(device) {        
        view.log("scanner: scan");
        // reset device list
        devicelist.clear();
        // scan for devices (all services, call on succes, call on failure)
        ble.startScan(
            [],
            scanner.success,
            function() {
                view.log("scanner: failed");
                app.react("scan failed");
            }
        );
        // stop scanning after 'scanTimeout' ms
        scanner.timeoutId = setTimeout(
            ble.stopScan,
            scanner.scanTimeout,
            function() {
                view.log("scan complete after " + scanner.scanTimeout + " ms");
                app.react("finished");
            },
            function() {
                view.log("scanner: failed");
                app.react("scan failed");
            }
        );
    },

    success : function(device) {
        view.log("scan successful: " + device.id);
        var listItem = document.createElement('li');
        var listHtml = '<b>NAME: ' + device.name + '</b><br/>' + 'RSSI: ' + device.rssi + '&nbsp;|&nbsp;' + device.id;
        listItem.dataset.deviceId = device.id;
        listItem.innerHTML = listHtml;
        devicelist.add(listItem);
    },
    
    interrupt : function() {
        ble.stopScan(
            function() {
                clearTimeout(scanner.timeoutId);
                view.log("scan successfully interrupted");
                app.react("interrupt finished");
            },
            function() {
                view.log("scan interruption failed");
                app.react("interrupt failed");
            }
        );
    }
}