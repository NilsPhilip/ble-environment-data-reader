
// Start listening for the deviceready-Event.
document.addEventListener("deviceready", onDeviceReady, false);

// Event received. We may now use PhoneGap APIs.
function onDeviceReady() {
    app.initialize();
}

function State(name, type, handler, enterHandler, leaveHandler) {
    this.name = name;
    this.type = type;
    this.enter = enterHandler;
    this.handle = handler;
    this.leave = leaveHandler;
}

var app = {
    
    contentOffsetTop : 0,
    
    ready : [],
    
    scanning : [],
    
    connecting : [],
    
    disconnecting : [],
        
    connected : [],
    
    scanningConnectRequest : [],
    
    initialize : function() {
        tabView.load("tabs", tabView.initialize); 
        app.contentOffsetTop = $("#connection-list").offset().top  
        app.state = app.ready;
        displays.state.showEvent(app.state.name, app.state.type);
    },
    
    react : function(message) {
        view.log(app.state.name + " now handles event " + message);
        app.state.handle(message);
        view.log(app.state.name + " handled " + message);
    },
    
    isConnected : function() {
        return app.state != null ? app.state == app.connected : false; 
    },
    
    changeState : function(newState) {
        displays.state.defaultEvent();
        app.state.leave();
        app.state = newState;
        app.state.enter();
        displays.state.showEvent(app.state.name, app.state.type);
    },
    
    initializeStates : function() {
        app.ready = new State(
            "device is ready",
            displays.state.display.defaultCompleteType,
            function(message) {     
                if (message == "scan") {
                    app.changeState(app.scanning);
                    scanner.refreshDeviceList();            
                } else if (message == "connect") {
                    app.changeState(app.connecting);
                    connection.connect();            
                } else if (message == "device select") {
                    buttons.connectBtn.activate();
                } else if (message == "device unselect") {
                    buttons.connectBtn.deactivate();
                }
            },
            function() {
                buttons.refreshBtn.activate();
                if(devicelist.hasSelected()) {
                    buttons.connectBtn.activate();    
                } else {
                    buttons.connectBtn.deactivate();  
                }
                buttons.disconnectBtn.deactivate();
            },
            function() { }
        );
        
        app.scanning = new State(
            "scanning",
            displays.state.display.defaultStartType,
            function(message) {
                if (message == "stop scanning") {
                    scanner.interrupt();
                } else if (message.endsWith("finished")) { 
                    displays.event.completion("scan finished");
                    app.changeState(app.ready);
                } else if (message == "connect") {
                    app.changeState(app.scanningConnectRequest);
                    scanner.interrupt();
                } else if (message.startsWith("scan failed")) {
                    app.changeState(app.ready);
                    displays.event.failure(message.replace("scan failed: ", ""));
                } else if (message == "interrupt failed") {
                    displays.event.failure("cannot stop scanning now");
                } else if (message == "device select") {
                    buttons.connectBtn.activate();
                } else if (message == "device unselect") {
                    buttons.connectBtn.deactivate();
                }
            },
            function() {
                buttons.refreshBtn.setName("stop scanning");
                buttons.refreshBtn.activate(
                    {
                        type : "click",
                        func : function() { app.react("stop scanning"); }
                    }
                );
                buttons.connectBtn.deactivate();
                buttons.disconnectBtn.deactivate();
            },
            function() {
                buttons.refreshBtn.setName("scan for devices");
                buttons.refreshBtn.activate(
                    {
                        type : "click",
                        func : function() { app.react("scan"); }
                    }
                );
            }
        );

        app.scanningConnectRequest = new State(
            "scanning",
            displays.state.display.defaultStartType,
            function(message) {
                if (message == "interrupt finished") {
                    app.changeState(app.connecting);
                    connection.connect();
                } else if (message == "interrupt failed") {
                    app.changeState(app.scanning);
                    displays.event.failure("cannot connect now");
                } else {
                    app.changeState(app.scanning);
                    app.react(message);
                }
            },
            function() {
                devicelist.lock();
            },
            function() {
                devicelist.unlock();
            }
        );

        app.connecting = new State(
            "connecting",
            displays.state.display.defaultStartType,
            function(message) {
                if (message == "failure") {
                    app.changeState(app.ready);
                    displays.event.failure("connection failed");
                } else if (message == "connected") {
                    app.changeState(app.connected);
                } else if (message == "disconnect") {
                    app.changeState(app.disconnecting);
                    displays.event.starting("disconnecting");
                    connection.disconnect();
                }
            },
            function() {
                devicelist.lock();
                buttons.refreshBtn.deactivate();
                buttons.connectBtn.deactivate();
                buttons.disconnectBtn.deactivate();
            },
            function() {
                devicelist.unlock();
                buttons.connectBtn.activate();
            }
        );

        app.connected = new State(
            "connected",
            displays.state.display.defaultCompleteType,
            function(message) {
                /*if (message == "add measurement") {
                    // add measurement to list
                    measurementlist.add(sensorListener.getMeasurement());
                    displays.event.completion("took measurement");
                } else*/ if (message == "disconnect") {
                    app.changeState(app.disconnecting);
                    connection.disconnect();
                } else if (message == "failure") {
                    // ble.connect call failure method if
                    // established connection aborts, too.
                    app.changeState(app.disconnecting);
                    app.react("not connected");
                }
            },
            function() {
                devicelist.lock();
                buttons.refreshBtn.deactivate();
                buttons.connectBtn.deactivate();
                buttons.disconnectBtn.activate();
            },
            function() {
                devicelist.unlock();
            }
        );

        app.disconnecting = new State(
            "disconnecting",
            displays.state.display.defaultStartType,
            function(message) {
                if (message == "disconnected") {
                    app.changeState(app.ready);
                    displays.event.completion("disconnected");
                } else if (message == "failure") {
                    app.changeState(app.connected);
                    displays.event.failure("disconnect request failed");
                } else if (message == "not connected") {
                    app.changeState(app.ready);
                    displays.event.failure("not connected anymore");
                }
            },
            function() {
                devicelist.lock();
                buttons.refreshBtn.deactivate();
                buttons.connectBtn.deactivate();
                buttons.disconnectBtn.deactivate();
            },
            function() {
                devicelist.unlock();
            }
        );
    }
}