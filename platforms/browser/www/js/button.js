
/* global index.html : refreshBtn, disconnectBtn, connectBtn */

/* global index.css : greyout */

var buttons = {
    
    initialize : function() {
        buttons.refreshBtn = new Button("refreshBtn", "scan for devices");
        buttons.disconnectBtn = new Button("disconnectBtn", "disconnect");
        buttons.connectBtn = new Button("connectBtn", "connect");
        buttons.addMeasurementBtn = new Button("addMeasurementBtn", "connect");
        buttons.removeMeasurementBtn = new Button("removeMeasurementBtn", "connect");
        buttons.sendMeasurementBtn = new Button("sendMeasurementBtn", "connect");
        buttons.createDbBtn = new Button("createDbBtn", "connect");
        buttons.postQueryBtn = new Button("postQueryBtn", "connect");
        buttons.getQueryBtn = new Button("getQueryBtn", "connect");
    }
}

/**
 * TODO: more than one listener
 *
 * btnId : String
 * activeText : String
 * func : function
 */
function Button(btnId, activeText) {
	this.id = btnId;
	this.element = document.getElementById(this.id);
    this.active = activeText;
	this.hidden = activeText;
    this.listener = [];
    
	this.activate = function(newListener) {
        if(arguments.length == 1) {
            this.register(newListener);
        } else {
            this.register(this.listener);
        }
		this.show();
	};
    
    this.deactivate = function(newListener) {
        if(arguments.length == 1) {
            this.register(newListener);
        }
        this.deregister();
		this.hide();
	};
    
	/**
	 * add a listener.
     *
     * example format: var listener = {type : "click", func : function() { }};
	 */
	this.register = function(newListener) {
        this.deregister();
        this.listener = newListener;
		this.element.addEventListener(
                newListener["type"],
                newListener["func"]
		);
	};
	/**
	 * remove a listener.
	 */
	this.deregister = function() {
		this.element.removeEventListener(
                this.listener["type"],
                this.listener["func"]
		);
	};
    
    this.show = function() {
		$("#" + this.id + " a").removeClass("greyout");
	};
    
    this.hide = function() {
		$("#" + this.id + " a").addClass("greyout");
	};

	this.text = function() {
		return this.active;
	};
    
	this.getHiddenText = function() {
		return this.hide;
	};

    /**
	 * change the text of the button.
     * animation fadeOut and fadeIn
	 * 
	 * newName : String
	 */
	this.changeName = function(newName) {
		// fadeout out animation to let user know something has changed
		$("#" + this.id).fadeOut();
		this.setName(newName);
		$("#" + this.id).fadeIn();
	};
    
    /**
	 * set the text of the button.
	 * 
	 * newName : String
	 */
	this.setName = function(newName) {
		$("#" + this.id + " a").text(newName);
	};
    
    this.setHiddenText = function(hiddenText) {
		this.hidden = hiddenText;
	};
    
    this.setActiveText = function(activeText) {
		this.active = activeText;
	};
}