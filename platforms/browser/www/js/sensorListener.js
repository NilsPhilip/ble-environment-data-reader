
var sensorListener = {
    
    measurement : [],
    
    getMeasurement : function() {
        var listItem = document.createElement('li');
        var time = new Date().timestamp();
        listItem.dataset.measurementId = time;
        listItem.dataset.temp = sensorListener.measurement["temp"];
        listItem.dataset.press = sensorListener.measurement["press"];
        listItem.dataset.hum = sensorListener.measurement["hum"];
        listItem.dataset.dust = sensorListener.measurement["dust"];
        listItem.dataset.co = sensorListener.measurement["co"];
        listItem.dataset.nh3 = sensorListener.measurement["nh3"];
        listItem.dataset.no2 = sensorListener.measurement["no2"];
        var listHtml = '<b>Time: ' + time + '</b><br/>'
                        + 'Temp: ' + listItem.dataset.temp + "<br/>"
                        + 'Press: ' + listItem.dataset.press + "<br/>"
                        + 'Hum: ' + listItem.dataset.hum + "<br/>"
                        + 'Dust: ' + listItem.dataset.dust + "<br/>"
                        + 'Co: ' + listItem.dataset.co + "<br/>"
                        + 'Nh3: ' + listItem.dataset.nh3 + "<br/>"
                        + 'No2: ' + listItem.dataset.no2;
        listItem.innerHTML = listHtml;
        return listItem;
    },
    
    copy : function(msrmt) {
        var listItem = document.createElement('li');
        var time = msrmt.dataset["measurementId"];
        listItem.dataset.measurementId = time;
        listItem.dataset.temp = msrmt.dataset["temp"];
        listItem.dataset.press = msrmt.dataset["press"];
        listItem.dataset.hum = msrmt.dataset["hum"];
        listItem.dataset.dust = sensorListener.measurement["dust"];
        listItem.dataset.co = sensorListener.measurement["co"];
        listItem.dataset.nh3 = sensorListener.measurement["nh3"];
        listItem.dataset.no2 = sensorListener.measurement["no2"];
        var listHtml = '<b>Time: ' + time + '</b><br/>'
                        + 'Temp: ' + msrmt.dataset.temp + "<br/>"
                        + 'Press: ' + msrmt.dataset.press + "<br/>"
                        + 'Hum: ' + msrmt.dataset.hum + "<br/>"
                        + 'Dust: ' + listItem.dataset.dust + "<br/>"
                        + 'Co: ' + listItem.dataset.co + "<br/>"
                        + 'Nh3: ' + listItem.dataset.nh3 + "<br/>"
                        + 'No2: ' + listItem.dataset.no2;
        listItem.innerHTML = listHtml;
        return listItem;
    },
    
    measurementToString : function(msrmt) {
        return "temp=" + msrmt.dataset.temp
                + ",hum=" + msrmt.dataset.hum
                + ",press=" + msrmt.dataset.press
                + ",dust=" + msrmt.dataset.dust
                + ",co=" + msrmt.dataset.co
                + ",nh3=" + msrmt.dataset.nh3
                + ",no2=" + msrmt.dataset.no2;
    },
    
    measurementFromString : function(str) {
        var splitted = str.split(",");
        var tuple;
        var msrmt = [];
        for(var i = 0; i < splitted.length; i++) {
            tuple = splitted[i].split("=");
            msrmt[tuple[0]] = tuple[1];
        }
        return msrmt;
    },
    
    receiveTemperature : function(buffer) {
        var value = new Int16Array(buffer);
        var floatVal = value[0] / 100.0;
        sensorListener.measurement["temp"] = floatVal;
    },

    receiveHumidity : function(buffer) {
        var value = new Uint16Array(buffer);
        var floatVal = value[0] / 100.0;
        sensorListener.measurement["hum"] = floatVal;
    },

    receivePressure : function(buffer) {
        var value = new Uint32Array(buffer);
        var floatVal = value[0] / 10.0;
        sensorListener.measurement["press"] = floatVal;
    },
    
    receiveDust : function(buffer) {
        var value = new Uint16Array(buffer);
        var floatVal = value[0] / 10.0;
        sensorListener.measurement["press"] = floatVal;
    },

    co2raw : 0,
    
    receiveCoRaw : function(buffer) {
        sensorListener.co2raw = new Uint16Array(buffer)[0];
    },
    
    receiveCoCalib : function(buffer) {
        var calib = new Uint16Array(buffer)[0];
        var value = Math.pow(ratio(sensorListener.co2raw,calib), -1.179)*4.385; 
        sensorListener.measurement["co"] = value;
    },
    
    nh3raw : 0,
    
    receiveNh3Raw : function(buffer) {
        sensorListener.nh3raw = new Uint16Array(buffer)[0];
    },
    
    receiveNh3Calib : function(buffer) {
        var calib = new Uint16Array(buffer)[0];
        var value = Math.pow(ratio(sensorListener.nh3raw,calib), 1.007)/6.855; 
        sensorListener.measurement["nh3"] = value;
    },
    
    no2raw : 0,
    
    receiveNo2Raw : function(buffer) {
        sensorListener.no2raw = new Uint16Array(buffer)[0];
    },
    
    receiveNo2Calib : function(buffer) {
        var calib = new Uint16Array(buffer)[0];
        var value = Math.pow(ratio(sensorListener.no2raw,calib), -1.67)/1.47; 
        sensorListener.measurement["no2"] = value;
    },
    
    receiveFailure : function() {
        displays.event.starting("1+ notify failed");
    },
    
    clear : function() {
        sensorListener.measurement = [];
    }    
}

/* http://stackoverflow.com/questions/10211145 */
Date.prototype.timestamp = function () {
     return ((this.getHours() < 10)?"0":"") + this.getHours() +"_"+ ((this.getMinutes() < 10)?"0":"") + this.getMinutes() +"_"+ ((this.getSeconds() < 10)?"0":"") + this.getSeconds();
}

function ratio(raw, calib) {
    return raw/calib;
}