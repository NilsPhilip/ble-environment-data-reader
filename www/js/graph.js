
/* global index.html : myChart */
 
var graph = {
    
    ctx : document.getElementById("myChart"),
    
    updateInterval : 3000,
    
    updateIntervalId : [],
    
    updateIntervalSet : false,
    
    nrOfSets : 7,
    
    max : 25,
    
    data :
        {
            labels: ["", "", "", "", "", "", "", "", "", ""],
            datasets: [
                {
                    label: "Temperature °C",
                    data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                    backgroundColor: "rgba(255, 99, 132, 0.2)",
                    borderColor: "rgba(255,99,132,1)",
                    borderWidth: 1
                },
                {
                    label: "Humidity %",
                    data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                    backgroundColor: "rgba(54, 162, 235, 0.2)",
                    borderColor: "rgba(54, 162, 235, 1)",
                    borderWidth: 1
                },
                {
                    label: "Pressure mPa",
                    data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                    backgroundColor: "rgba(255, 206, 86, 0.2)",
                    borderColor: "rgba(255, 206, 86, 1)",
                    borderWidth: 1
                },
                {
                    label: "Dust μg/m^3",
                    data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                    backgroundColor: "rgba(75, 192, 192, 0.2)",
                    borderColor: "rgba(75, 192, 192, 1)",
                    borderWidth: 1
                },
                {
                    label: "Co ppm",
                    data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                    backgroundColor: "rgba(153, 102, 255, 0.2)",
                    borderColor: "rgba(153, 102, 255, 1)",
                    borderWidth: 1
                },
                {
                    label: "Nh3 ppm",
                    data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                    backgroundColor: "rgba(255, 159, 64, 0.2)",
                    borderColor: "rgba(255, 159, 64, 1)",
                    borderWidth: 1
                },
                {
                    label: "No2 ppm",
                    data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                    backgroundColor: "rgba(255, 106, 100, 0.2)",
                    borderColor: "rgba(255, 106, 100, 1)",
                    borderWidth: 1
                }
            ]
        },
        
    myChart : [],
    
    load : function() {
        graph.myChart = new Chart(graph.ctx, {
            type: "line",
            data: graph.data,
            options: {
                maintainAspectRatio: true,
                responsive: true
            }
        });
    },
    
    isOn : function() {
        return graph.updateIntervalSet;
    },
    
    on : function() {
        graph.off();
        
        if(measurementlist.hasSelected()) {
            var selected = measurementlist.getSelected();
            graph.resetData(selected.length); // set size + all points to zero
            for(var i = 0; i < selected.length; i++) {
                // the data set of the html measurement element is not to confuse
                // with the data set of the graph
                graph.addMeasurement(selected[i].dataset, true);
            }
            graph.myChart.update();
            view.log("graph: measurement displayed");
        } else if(app.isConnected()) {
            graph.updateIntervalId = setInterval(
                function() {
                    graph.addMeasurement(sensorListener.measurement);
                    graph.myChart.update();
                },
                graph.updateInterval
            );
            graph.updateIntervalSet = true;
            view.log("graph: set update interval to " + graph.updateInterval + " ms");
        } else {
            graph.resetData(10);
            graph.myChart.update();
            displays.event.starting("no measurements to display");
        }
    },
    
    off : function() {
        clearInterval(graph.updateIntervalId);
        graph.updateIntervalSet = false;
        view.log("graph turned off");
    },
    
    addMeasurement : function(measurement, shift) {
        var sensors = ["temp", "hum", "press", "dust", "co", "nh3", "no2"];
        shift = arguments.length == 1 ? false : shift;
        for(var i = 0; i < graph.nrOfSets; i++) {
            graph.addDataPoint(i, measurement[sensors[i]], shift);
        }     
    },
    
    getDataset : function(index) {
        return graph.data["datasets"][index];
    },

    getData : function(index) {
        return graph.getDataset(index)["data"];
    },
    
    setData : function(index, newData) {
        newData = arguments.length == 1 ? [0, 0, 0, 0, 0, 0, 0, 0, 0, 0] : newData;
        graph.data["datasets"][index].data = copy(newData);
    },
    
    resetData : function(newSize) {
        var newData = [];
        for(var i = 0; i < newSize; i++) {
            newData.push(0);
        }
        for(var i = 0; i < graph.nrOfSets; i++) {
            graph.setData(i, copy(newData));
        }
        graph.setXAxis(newSize);
    },
    
    trimData : function(newSize) {
        if(newSize < graph.getXAxis()) {
            // trim data
            for(var i = 0; i < graph.nrOfSets; i++) {
                var tempData = graph.getData(i);
                var copy = [];
                for(var j = tempData.length-newSize; j < tempData.length; j++) {
                    copy.push(tempData[j]);
                }
                graph.setData(i, copy);
            }
        } else if (newSize > graph.getXAxis()) {
            // expand data
            for(var i = 0; i < graph.nrOfSets; i++) {
                for(var k = graph.getXAxis(); k < newSize; k++) {
                    graph.addDataPoint(i, 0);
                }
            }
        }
    },
    
    addDataPoint : function(index, value, shift) {
        shift = arguments.length == 3 ? shift : false;
        shift = graph.getXAxis() < graph.max ? shift : true;
        
        graph.getData(index).push(value);
        if(shift) {
            graph.getData(index).shift();    
        } else if(graph.getDataLength() > graph.getXAxis()) {
            graph.setXAxis(graph.getXAxis() + 1);
        }
        view.log("added " + value + " to " + graph.getDataset(index).label);
        view.log("new value: " + graph.getData(index)[graph.getData(index).length-1] + " in" + graph.getDataset(index).label);
    },
    
    getDataLength: function() {
        return graph.getData(0).length;
    },
    
    getXAxis : function() {
        return graph.data["labels"].length;
    },
    
    setXAxis : function(newSize) {           
        var newLabels = [];
        for(var i = 0; i < newSize; i++) {
            newLabels[i] = "";
        }
        graph.data["labels"] = newLabels;
    }   
}

var simple = {
    
    updateInterval : 3000,
    
    updateIntervalSet : false,
    
    updateIntervalId : [],
    
    on : function() {
        simple.off();
        
        if(measurementlist.hasSelected()) {
            simple.resetData();
            var selected = measurementlist.getSelected();
            simple.addMeasurement(simple.average(selected));
            view.log("simple display: measurement displayed");
        } else if(app.isConnected()) {
            simple.updateIntervalId = setInterval(
                function() {
                    simple.addMeasurement(sensorListener.measurement);
                },
                simple.updateInterval
            );
            simple.updateIntervalSet = true;
            view.log("simple display: set update interval to " + simple.updateInterval + " ms");
        } else {
            simple.resetData();
            displays.event.starting("no measurements to display");
        }
    },
    
    off : function() {
        clearInterval(simple.updateIntervalId);
        simple.updateIntervalSet = false;
        view.log("simple display turned off");
    },
    
    addMeasurement : function(measurement) {
        var sensors = ["temp", "hum", "press", "dust", "co", "nh3", "no2"];
        for(var i = 0; i < 7; i++) {
            simple.setValue(i, measurement[sensors[i]]);
        }
        view.log("simple display: pushed values");
    },
    
    resetData : function() {
        for(var i = 0; i < 7; i++) {
            simple.setValue(i, 0.0);
        }
        view.log("simple display: reset");
    },
    
    setValue : function(index, value) {
        var displayIds = ["#deviceTemp", "#deviceHum", "#devicePress", "#deviceDust", "#deviceCo", "#deviceNh3", "#deviceNo2"];
        var displayNames = ["Temperature", "Humidity", "Pressure", "Dust", "Co", "Nh3", "No2"];
        var displayUnits = ["°C", "%", " mPa", " ppm", " ppm", " ppm"];
        
        if(value == 0.0) {
            $(displayIds[index]).html("No " + displayNames[index]);
            
        } else {
            $(displayIds[index]).html(displayNames[index] + ": " + value + "" + displayUnits[index]);
        }
    },
    
    average : function(measurements) {
        var sensors = ["temp", "hum", "press", "dust", "co", "nh3", "no2"];
        var avgMeasurement = [];
        for(var i = 0; i < sensors.length; i++) {
            avgMeasurement[sensors[i]] = 0;
        }
        
        for(var i = 0; i < measurements.length; i++) {
            for(var j = 0; j < sensors.length; j++) {
                avgMeasurement[sensors[j]] += parseFloat(measurements[i].dataset[sensors[j]]);
            }
        }
        for(var i = 0; i < sensors.length; i++) {
            avgMeasurement[sensors[i]] = avgMeasurement[sensors[i]]/measurements.length;
        }
        return avgMeasurement;
    }
}

function copy(arr) {
    var copy = [];
    for(var j = 0; j < arr.length; j++) {
        copy[j] = arr[j];
    }
    return copy;
}