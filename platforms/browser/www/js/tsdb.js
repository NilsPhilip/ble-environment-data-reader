
// URLs of API endpoints
var REST_API_URL_CREATE = "http://mociotdb2.teco.edu/db.php/createDB?dbName=";
var REST_API_URL_WRITE = "http://mociotdb2.teco.edu/db.php/write/";
var REST_API_URL_READ = "http://mociotdb2.teco.edu/db.php/read/";

var tsdb = {
    
    ready : false,
    
    id : "",
    
    name : "",
    
    pw : "",
    
    setName : function(newName) {
        tsdb.name = newName;
    },
    
    setPassword : function(newPassword) {
        tsdb.pw = newPassword;
    },
    
    setId : function(newId) {
        tsdb.id = newId;
    },
    
    getName : function() {
        return tsdb.name;
    },
    
    getPassword : function() {
        return "dbKey=" + tsdb.pw;
    },
    
    getId : function() {
        return tsdb.id;
    },
    
    hasId : function() {
        return tsdb.id != "" && isAlphaNumeric(tsdb.id);
    },

    createDatabase : function() {
        if (tsdb.isReady()) {
            $.ajax({
                type: "GET",
                url: REST_API_URL_CREATE + tsdb.getName() + "&" + tsdb.getPassword(),
                success: (function(data, code, xhr) {
                    $('#reply').text('Reply: ' + JSON.stringify(data));
                    displays.event.completion("created data base: " + xhr.status);
                }),
                error: (function(xhr, error) {
                    $('#reply').text('Reply: ' + error);
                    displays.event.failure("creation failed: " + xhr.status);
                })
            });		
        }
    },

    writeData : function() {
        if (tsdb.isReady()) {
            if(tsdb.hasId()) {
                if(tsdblist.hasSelected()) {
                    var msrmtToSend = tsdblist.getSelected()[0];
                    var toSend = "sensorValues id=" + tsdb.getId() + "," + sensorListener.measurementToString(msrmtToSend);
                    var thisUrl = REST_API_URL_WRITE + tsdb.getName() + "?" + tsdb.getPassword();
                    $.ajax({
                        type: "POST",
                        url: thisUrl,
                        data: toSend,
                        success: (function(data, code, xhr) {
                            $('#reply').text('Reply: ' + JSON.stringify(data));
                            tsdblist.clear();
                            displays.event.completion("wrote data to db: " + xhr.status);
                        }),
                        error: (function(xhr, error) {
                            $('#reply').text('Reply: ' + error);
                            displays.event.failure("writing data failed: " + xhr.status);
                        })
                    });	
                } else {
                    displays.event.starting("select a measurement first");
                }   
            } else {
                displays.event.starting("specify a name first");
            }
        }	
    },

    readDataAndDisplay : function() {
        if (tsdb.isReady()) {
            if(tsdb.hasId()) {
                var thisUrl = REST_API_URL_READ + tsdb.getName() + "?" + tsdb.getPassword() + "&q=select * from sensorValues";
                alert(thisUrl);
                $.ajax({
                    type: "GET",
                    url: thisUrl,
                    success: (function(data, code, xhr) {
                        $('#reply').text('Reply: ' + JSON.stringify(data));
                        var msrmt = sensorListener.measurementFromString(data);
                        measurementlist.add(msrmt);
                        displays.event.completion("read data from db: " + xhr.status);
                    }),
                    error: (function(xhr, error) {
                        $('#reply').text('Reply: ' + error);
                         displays.event.failure("reading data failed: " + xhr.status);
                    })
                });
            } else {
                displays.event.starting("specify a name first");
            }            
        }
    },
    
    isReady : function() {
        if(tsdb.name == "" || tsdb.pw == "") {
            displays.event.starting("enter name/pw first");
        } else if(isAlphaNumeric(tsdb.name) && isAlphaNumeric(tsdb.pw)) {
            return true;
        } else {
            displays.event.failure("name/pw not alphanumeric");
        }
    }
}

var tsdblist = {
    
    size : 0,
    
    initialize : function() {
        tsdblist.clear();
        $("#tsdb-list-body").on(
            "click",
            "li",
            function() {
                if($(this).hasClass("highlight")) {
                    $("#tsdb-list-body li").removeClass("highlight");
                    view.log("tsdb msrmt unselected");
                } else {
                    $("#tsdb-list-body li").removeClass("highlight");
                    $(this).addClass("highlight");
                    view.log("tsdb msrmt selected");
                }
            }
        );
    },
    
    getSelected : function() {
        var selected = [];
        var i = 0;
        $("#tsdb-list-body").children().each(
            function(index)
                {
                    if($(this).hasClass("highlight")) {
                        selected[i++] = this;
                    }
                }
        );
        return selected;
    },
    
    hasSelected : function() {
        var hasSelected = false;
        
        $("#tsdb-list-body").children().each(
            function(index)
                {
                    if($(this).hasClass("highlight")) {
                        hasSelected = true;
                    }
                }
        );
        return hasSelected;
    },
    
    add : function(item) {
        if (tsdblist.isEmpty()) {
            $("#tsdb-list-body").html("");
        }
        $("#tsdb-list-body").append(item);
        tsdblist.size++;
    },
    
    addAll : function(items) {
        for(var i = 0; i < items.length; i++) {
            tsdblist.add(items[i]);
        }
    },
    
    remove : function() {
        $("#tsdb-list-body").children().each(
            function(index)
                {
                    if($(this).hasClass("highlight")) {
                        $(this).remove();
                        tsdblist.size--;
                    }
                }
        );
        if (tsdblist.isEmpty()) {
            $("#tsdb-list-body").html("List Empty");
            
        }
    },
    
    clear : function() {
        $("#tsdb-list-body").html("List Empty");
        tsdblist.size = 0;
    },
    
    isEmpty : function() {
        return tsdblist.size == 0;
    },
    
    height : function(newHeight) {
        $("#tsdb-list").height(newHeight);
        $("#tsdb-list-body").height(newHeight);
    }
}

function isAlphaNumeric(str) {
  var code, i, len;

  for (i = 0, len = str.length; i < len; i++) {
    code = str.charCodeAt(i);
    if (!(code > 47 && code < 58) && // numeric (0-9)
        !(code > 64 && code < 91) && // upper alpha (A-Z)
        !(code > 96 && code < 123)) { // lower alpha (a-z)
      return false;
    }
  }
  return true;
};