
/* global index.html : myChart */
 
var graph = {
    
    ctx : document.getElementById("myChart"),
    
    updateInterval : 3000,
    
    updateIntervalId : [],
    
    updateIntervalSet : false,
    
    nrOfSets : 7;
    
    data :
        {
            labels: ["", "", "", "", "", "", "", "", "", ""],
            datasets: [
                {
                    label: "Temperature °C",
                    data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                    backgroundColor: "rgba(255, 99, 132, 0.2)",
                    borderColor: "rgba(255,99,132,1)",
                    borderWidth: 1
                },
                {
                    label: "Humidity %",
                    data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                    backgroundColor: "rgba(54, 162, 235, 0.2)",
                    borderColor: "rgba(54, 162, 235, 1)",
                    borderWidth: 1
                },
                {
                    label: "Pressure Pa (1000)",
                    data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                    backgroundColor: "rgba(255, 206, 86, 0.2)",
                    borderColor: "rgba(255, 206, 86, 1)",
                    borderWidth: 1
                },
                {
                    label: "Dust μg/m^3",
                    data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                    backgroundColor: "rgba(75, 192, 192, 0.2)",
                    borderColor: "rgba(75, 192, 192, 1)",
                    borderWidth: 1
                },
                {
                    label: "Co ppm",
                    data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                    backgroundColor: "rgba(153, 102, 255, 0.2)",
                    borderColor: "rgba(153, 102, 255, 1)",
                    borderWidth: 1
                },
                {
                    label: "Nh3 ppm",
                    data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                    backgroundColor: "rgba(255, 159, 64, 0.2)",
                    borderColor: "rgba(255, 159, 64, 1)",
                    borderWidth: 1
                },
                {
                    label: "No2 ppm",
                    data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                    backgroundColor: "rgba(255, 106, 100, 0.2)",
                    borderColor: "rgba(255, 106, 100, 1)",
                    borderWidth: 1
                }
            ]
        },
        
    myChart : [],
    
    setData : function(index, newData) {
        if(arguments.length == 0) {
            for(var i = 0; i < graph.nrOfSets; i++) {
                graph.setData(i, [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]);
            }
        } else if(arguments.length == 1) {
            graph.setData(index, [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]);
        } else if(arguments.length == 2) {
            graph.data["datasets"][index].data = newData;   
        }
    },
    
    load : function() {
        graph.myChart = new Chart(graph.ctx, {
            type: "line",
            data: graph.data,
            options: {
                maintainAspectRatio: true,
                responsive: true,
                scales: {
                    yAxes: [{
                        ticks: {
                            min: 0
                        }
                    }]
                }
            }
        });
    },
    
    isOn : function() {
        return graph.updateIntervalSet;
    },
    
    on : function() {
        graph.off();
        
        if(measurementlist.hasSelected()) {
            graph.setData();
            var selected = measurementlist.getSelected();
            for(var i = 0; i < selected.length; i++) {
                graph.addMeasurement(selected[i].dataset);
                graph.myChart.update();
            }
            view.log("graph: measurement displayed");
        } else {
            graph.updateIntervalId = setInterval(
                function() {
                    graph.addMeasurement(sensorListener.measurement);
                    graph.myChart.update();
                },
                graph.updateInterval
            );
            graph.updateIntervalSet = true;
            view.log("graph: set update interval to " + graph.updateInterval + " ms");
        }
    },
    
    off : function() {
        clearInterval(graph.updateIntervalId);
        graph.updateIntervalSet = false;
        view.log("graph turned off");
    },
    
    addMeasurement : function(measurement) {
        var sensors = ["temp", "hum", "press", "dust", "co", "nh3", "no2"];
        
        for(var i = 0; i < graph.nrOfSets; i++) {
            graph.addDataPoint(graph.getDataset(i), measurement[sensors[i]]);
        }     
    },
    
    getDataset : function(nr) {
        return graph.data["datasets"][nr];
    },
    
    addDataPoint : function(dataset, value) {
        view.log("push " + value + " to " + dataset.label);
        dataset["data"].push(value);
        dataset["data"].shift();
    }
}

var simple = {
    
    updateInterval : 3000,
    
    updateIntervalSet : false,
    
    updateIntervalId : [],
    
    on : function() {
        simple.off();
        
        if(measurementlist.hasSelected()) {
            var selected = measurementlist.getSelected();
            simple.addMeasurement(selected[0].dataset);
            view.log("simple display: measurement displayed");
        } else {
            simple.updateIntervalId = setInterval(
                function() {
                    simple.addMeasurement(sensorListener.measurement);
                },
                simple.updateInterval
            );
            simple.updateIntervalSet = true;
            view.log("simple display: set update interval to " + simple.updateInterval + " ms");
        }
    },
    
    off : function() {
        clearInterval(simple.updateIntervalId);
        simple.updateIntervalSet = false;
        view.log("simple display turned off");
    },
    
    addMeasurement : function(measurement) {
        view.log("simple display: push");
        $("#deviceTemp").html("Temperature: " + measurement["temp"] + "°C");
        $("#deviceHum").html("Humidity: " + measurement["hum"] + "%");
        $("#devicePress").html("Pressure: " + measurement["press"] + " Pa");
        $("#deviceDust").html("Dust: " + measurement["dust"] + " mu g/m^3");
        $("#deviceCo").html("Co: " + measurement["co"] + " ppm");
        $("#deviceNh3").html("Nh3: " + measurement["nh3"] + " ppm");
        $("#deviceNo2").html("No2: " + measurement["no2"] + " ppm");
    }
}