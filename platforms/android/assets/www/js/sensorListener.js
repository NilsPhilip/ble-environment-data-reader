
var sensorListener = {
    
    measurement : [],
    
    initialize : function() {
        // set initial values to avoid 'undefined',
        // which hinders chart to display values.
        sensorListener.clear();
    },
    
    getMeasurement : function() {
        // measurement object to measurement html
        var listItem = document.createElement('li');
        var time = getDateTime();
        listItem.dataset.time = time;
        listItem.dataset.temp = sensorListener.measurement["temp"];
        listItem.dataset.press = sensorListener.measurement["press"];
        listItem.dataset.hum = sensorListener.measurement["hum"];
        listItem.dataset.dust = sensorListener.measurement["dust"];
        sensorListener.getCo();
        listItem.dataset.co = sensorListener.measurement["co"];
        sensorListener.getNh3();
        listItem.dataset.nh3 = sensorListener.measurement["nh3"];
        sensorListener.getNo2();
        listItem.dataset.no2 = sensorListener.measurement["no2"];
        var listHtml = '<b>Time: ' + time + '</b><br/>'
                        + 'Temp: ' + listItem.dataset.temp + "<br/>"
                        + 'Press: ' + listItem.dataset.press + "<br/>"
                        + 'Hum: ' + listItem.dataset.hum + "<br/>"
                        + 'Dust: ' + listItem.dataset.dust + "<br/>"
                        + 'Co: ' + listItem.dataset.co + "<br/>"
                        + 'Nh3: ' + listItem.dataset.nh3 + "<br/>"
                        + 'No2: ' + listItem.dataset.no2;
        listItem.innerHTML = listHtml;
        return listItem;
    },
    
    copy : function(msrmt) {
        // copy html measurement to html measurement
        var listItem = document.createElement('li');
        listItem.dataset.time = msrmt.dataset["time"];
        listItem.dataset.temp = msrmt.dataset["temp"];
        listItem.dataset.press = msrmt.dataset["press"];
        listItem.dataset.hum = msrmt.dataset["hum"];
        listItem.dataset.dust = msrmt.dataset["dust"];
        listItem.dataset.co = msrmt.dataset["co"];
        listItem.dataset.nh3 = msrmt.dataset["nh3"];
        listItem.dataset.no2 = msrmt.dataset["no2"];
        var listHtml = '<b>Time: ' + msrmt.dataset.time + '</b><br/>'
                        + 'Temp: ' + msrmt.dataset.temp + "<br/>"
                        + 'Press: ' + msrmt.dataset.press + "<br/>"
                        + 'Hum: ' + msrmt.dataset.hum + "<br/>"
                        + 'Dust: ' + listItem.dataset.dust + "<br/>"
                        + 'Co: ' + listItem.dataset.co + "<br/>"
                        + 'Nh3: ' + listItem.dataset.nh3 + "<br/>"
                        + 'No2: ' + listItem.dataset.no2;
        listItem.innerHTML = listHtml;
        return listItem;
    },
    
    measurementToString : function(msrmt) {
        return "time=\"" + msrmt.dataset.time + "\""
                + ",temp=" + msrmt.dataset.temp
                + ",hum=" + msrmt.dataset.hum
                + ",press=" + msrmt.dataset.press
                + ",dust=" + msrmt.dataset.dust
                + ",co=" + msrmt.dataset.co
                + ",nh3=" + msrmt.dataset.nh3
                + ",no2=" + msrmt.dataset.no2;
    },
    
    measurementFromString : function(str) {
        var splitted = str.split(",");
        var tuple;
        var msrmt = [];
        for(var i = 0; i < splitted.length; i++) {
            tuple = splitted[i].split("=");
            msrmt[tuple[0]] = tuple[1];
        }
        return msrmt;
    },
    
    receiveTemperature : function(buffer) {
        var value = new Int16Array(buffer);
        var floatVal = value[0] / 100.0;
        sensorListener.measurement["temp"] = floatVal;
    },

    receiveHumidity : function(buffer) {
        var value = new Uint16Array(buffer);
        var floatVal = value[0] / 100.0;
        sensorListener.measurement["hum"] = floatVal;
    },

    receivePressure : function(buffer) {
        var value = new Uint32Array(buffer);
        var floatVal = value[0] / 10000.0;
        sensorListener.measurement["press"] = floatVal;
    },
    
    receiveDust : function(buffer) {
        var value = new Uint16Array(buffer);
        var floatVal = value[0] / 1.0;
        var result = 2.7 * floatVal - 220;
        sensorListener.measurement["dust"] = result;
    },

    coRaw : 0,
    
    coCalib : 0,
    
    receiveCoRaw : function(buffer) {
        sensorListener.coRaw = new Uint16Array(buffer)[0] / 1.0;
    },
    
    receiveCoCalib : function(buffer) {
        sensorListener.coCalib = new Uint16Array(buffer)[0] / 1.0;
    },
    
    getCo : function() {
        if(sensorListener.coRaw != 0 && sensorListener.coCalib != 0) {
            var value = Math.pow(ratio(sensorListener.coRaw,sensorListener.coCalib), -1.179) * 4.385;
            sensorListener.coRaw = 0;
            sensorListener.coCalib = 0;
            sensorListener.measurement["co"] = value;                
        } else {
            sensorListener.measurement["co"] = 0.0;
        }
    },
    
    nh3Raw : 0,
    
    nh3Calib : 0,
    
    receiveNh3Raw : function(buffer) {
        sensorListener.nh3Raw = new Uint16Array(buffer)[0] / 1.0;
    },
    
    receiveNh3Calib : function(buffer) {
        sensorListener.nh3Calib = new Uint16Array(buffer)[0] / 1.0;
    },
    
    getNh3 : function() {
        if(sensorListener.nh3Raw != 0 && sensorListener.nh3Calib != 0) {
            var value = Math.pow(ratio(sensorListener.nh3Raw,sensorListener.nh3Calib), 1.007)/6.855;
            sensorListener.nh3Raw = 0;
            sensorListener.nh3Calib = 0;
            sensorListener.measurement["nh3"] = value;              
        } else {
            sensorListener.measurement["nh3"] = 0.0;
        }
    },
    
    no2Raw : 0,
    
    no2Calib : 0,
    
    receiveNo2Raw : function(buffer) {
        sensorListener.no2Raw = new Uint16Array(buffer)[0] / 1.0;
    },
    
    receiveNo2Calib : function(buffer) {
        sensorListener.no2Calib = new Uint16Array(buffer)[0] / 1.0;
    },
    
    getNo2 : function() {
        if(sensorListener.no2Raw != 0 && sensorListener.no2Calib != 0) {
            var value = Math.pow(ratio(sensorListener.no2Raw,sensorListener.no2Calib), -1.67)/1.47;
            sensorListener.no2Raw = 0;
            sensorListener.no2Calib = 0;
            sensorListener.measurement["no2"] = value;             
        } else {
            sensorListener.measurement["no2"] = 0.0;
        }
    },
    
    receiveTempFailure : function() {
        buttons.notTempBtn.activate();
        displays.event.starting("temperature notify failed");
    },
    
    receivePressFailure : function() {
        buttons.notPressBtn.activate();
        displays.event.starting("pressure notify failed");
    },
    
    receiveHumFailure : function() {
        buttons.notHumBtn.activate();
        displays.event.starting("humitidy notify failed");
    },
    
    receiveDustFailure : function() {
        buttons.notDustBtn.activate();
        displays.event.starting("dust notify failed");
    },
    
    receiveCoFailure : function() {
        buttons.notCoBtn.activate();
        connection.stopNotification("co raw", GAS_SERVICE, GAS_CO_RAW);
        connection.stopNotification("co calib", GAS_SERVICE, GAS_CO_CALIB);
        displays.event.starting("co notify failed");
    },
    
    receiveNh3Failure : function() {
        buttons.notNh3Btn.activate();
        connection.stopNotification("nh3 calib", GAS_SERVICE, GAS_NH3_CALIB);
        connection.stopNotification("nh3 raw", GAS_SERVICE, GAS_NH3_RAW);
        displays.event.starting("nh3 notify failed");
    },
    
    receiveNo2Failure : function() {
        buttons.notNo2Btn.activate();
        connection.stopNotification("no2 calib", GAS_SERVICE, GAS_NO2_CALIB);
        connection.stopNotification("no2 raw", GAS_SERVICE, GAS_NO2_RAW);
        displays.event.starting("no2 notify failed");
    },
    
    receiveFailure : function() {
        displays.event.starting("notification failed");
    },
    
    random : function() {
        view.log("random");
        sensorListener.measurement = [];
        sensorListener.measurement["time"] = "\"" + getDateTime() + "\"";
        sensorListener.measurement["temp"] = getRandom(10, 40);
        sensorListener.measurement["press"] = getRandom(90, 100);
        sensorListener.measurement["hum"] = getRandom(1, 100);
        sensorListener.measurement["dust"] = getRandom(1, 100);
        sensorListener.measurement["co"] = getRandom(1, 500);
        sensorListener.measurement["nh3"] = getRandom(1, 500);
        sensorListener.measurement["no2"] = getRandom(1, 500);
        view.log("random end");
        sleep(250 + Math.floor(Math.random() * 500 + 1));
    },
    
    clear : function() {
        sensorListener.measurement = [];
        sensorListener.measurement["time"] = getDateTime();
        sensorListener.measurement["temp"] = 0.0;
        sensorListener.measurement["press"] = 0.0;
        sensorListener.measurement["hum"] = 0.0;
        sensorListener.measurement["dust"] = 0.0;
        sensorListener.measurement["co"] = 0.0;
        sensorListener.measurement["nh3"] = 0.0;
        sensorListener.measurement["no2"] = 0.0;
    }    
}

function getDateTime() {
    return new Date().today() + "T" + new Date().timestamp() + "Z";
}

function ratio(raw, calib) {
    return raw/calib;
}

Date.prototype.today = function () { 
    return this.getFullYear() + "-"
         + (((this.getMonth()+1) < 10)?"0":"") + (this.getMonth()+1) +"-"
         + ((this.getDate() < 10)?"0":"") + this.getDate();
}

Date.prototype.timestamp = function () {
     return ((this.getHours() < 10)?"0":"") + this.getHours() +":"+ ((this.getMinutes() < 10)?"0":"") + this.getMinutes() +":"+ ((this.getSeconds() < 10)?"0":"") + this.getSeconds() + "."+
     (this.getMilliseconds());
}

function getRandom(min, max) {
	var vor = min + Math.floor(Math.random() * (max-min) + 1);
	var num = Math.random();
	var nach = Math.round(num * 100) / 100;
    return vor + nach;
}