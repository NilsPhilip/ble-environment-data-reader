
/* global index.html : deviceList, refreshBtn, disconnectBtn */

/* global button.js : refreshBtn, disconnectBtn */

/* Service/Characteristics for BME-Sensor. */
var ENV_SERVICE	       	= "181a";
var ENV_TEMP			= "2a6e";
var ENV_HUM				= "2a6f";
var ENV_PRESS			= "2a6d";

/* Service/Characteristics for MICS-Sensor. */
var GAS_SERVICE   		= "4b822f90-3941-4a4b-a3cc-b2602ffe0d00";
var GAS_CO_RAW			= "4b822fa1-3941-4a4b-a3cc-b2602ffe0d00";
var GAS_CO_CALIB		= "4b822fa2-3941-4a4b-a3cc-b2602ffe0d00";
var GAS_NO2_RAW			= "4b822f91-3941-4a4b-a3cc-b2602ffe0d00";
var GAS_NO2_CALIB		= "4b822f92-3941-4a4b-a3cc-b2602ffe0d00";
var GAS_NH3_RAW			= "4b822fb1-3941-4a4b-a3cc-b2602ffe0d00";
var GAS_NH3_CALIB		= "4b822fb2-3941-4a4b-a3cc-b2602ffe0d00";

var DUST_SERVICE   		= "4b822fe0-3941-4a4b-a3cc-b2602ffe0d00";
var DUST_RAW    		= "4b822fe1-3941-4a4b-a3cc-b2602ffe0d00";

var connection = {
    
    currentDevice : [],
    
    aborted : false,
    
    connect : function() {
        var deviceId = connection.currentDevice.deviceId;
    
        //// private helper functions ////
        
        var onConnect = function() {
            view.log("connected to " + deviceId);
            sensorListener.clear();
            connection.startNotification();
            app.react("connected");
        };
        
        var alreadyConnected = function() {
            view.log("already connected to " + deviceId);
            app.react("already connected"); // TODO
        };
        
        //// connect to device ////
        ble.isConnected(
            deviceId,
            alreadyConnected,
            function() {
                ble.connect(
                    deviceId,
                    onConnect,
                    function() {
                        app.react("failure");
                    }
                );
            }
        );
    },

    disconnect : function() {
        var deviceId = connection.currentDevice.deviceId;
        
        var disconnectFromCurrentDevice = function() {
            connection.stopNotification();
            ble.disconnect(deviceId, onDisconnect, onFailure);
        };
        
        var onDisconnect = function() {
            view.log("disconnected from " + deviceId);
            connection.currentDevice = [];
            app.react("disconnected");
        };
        
        var notConnected = function() {
            view.log("not connected");
            app.react("not connected"); // TODO
        };

        var onFailure = function(cause) {
            view.log("disconnection request failed");
            app.react("failure");
        };
        
        ble.isConnected(deviceId, disconnectFromCurrentDevice, notConnected);
    },
    
    startNotification : function(receiver, onFailure, name, service, characteristic) {
        if(arguments.length == 5) {
            var deviceId = connection.currentDevice.deviceId;
            view.log("starting " + name + " notification");
            ble.startNotification(
                deviceId,
                service, 
                characteristic,
                receiver,
                onFailure
            );
        } else {
            connection.startNotification(sensorListener.receiveTemperature, sensorListener.receiveFailure, "temperature", ENV_SERVICE, ENV_TEMP);
            connection.startNotification(sensorListener.receiveHumidity, sensorListener.receiveFailure, "humidity", ENV_SERVICE, ENV_HUM);
            connection.startNotification(sensorListener.receivePressure, sensorListener.receiveFailure, "pressure", ENV_SERVICE, ENV_PRESS);
            connection.startNotification(sensorListener.receiveDust, sensorListener.receiveFailure, "dust", DUST_SERVICE, DUST_RAW);
            connection.startNotification(sensorListener.receiveCoRaw, sensorListener.receiveFailure, "co raw", GAS_SERVICE, GAS_CO_RAW);
            connection.startNotification(sensorListener.receiveCoCalib, sensorListener.receiveFailure, "co calib", GAS_SERVICE, GAS_CO_CALIB);
            connection.startNotification(sensorListener.receiveNh3Calib, sensorListener.receiveFailure, "nh3 calib", GAS_SERVICE, GAS_NH3_CALIB);
            connection.startNotification(sensorListener.receiveNh3Raw, sensorListener.receiveFailure, "nh3 raw", GAS_SERVICE, GAS_NH3_RAW);
            connection.startNotification(sensorListener.receiveNo2Calib, sensorListener.receiveFailure, "no2 calib", GAS_SERVICE, GAS_NO2_CALIB);
            connection.startNotification(sensorListener.receiveNo2Raw, sensorListener.receiveFailure, "no2 raw", GAS_SERVICE, GAS_NO2_RAW);
        }
    },
    
    stopNotification : function(name, service, characteristic) {
        if(arguments.length == 3) {
            var deviceId = connection.currentDevice.deviceId;
            ble.stopNotification(
                deviceId,
                service,
                characteristic,
                function() { },
                function() { }
            );
        } else {
            connection.stopNotification("temperature", ENV_SERVICE, ENV_TEMP);
            connection.stopNotification("pressure", ENV_SERVICE, ENV_PRESS);
            connection.stopNotification("humidity", ENV_SERVICE, ENV_HUM);
            connection.stopNotification("dust", DUST_SERVICE, DUST_RAW);
            connection.stopNotification("co raw", GAS_SERVICE, GAS_CO_RAW);
            connection.stopNotification("co calib", GAS_SERVICE, GAS_CO_CALIB);
            connection.stopNotification("nh3 calib", GAS_SERVICE, GAS_NH3_CALIB);
            connection.stopNotification("nh3 raw", GAS_SERVICE, GAS_NH3_RAW);
            connection.stopNotification("no2 calib", GAS_SERVICE, GAS_NO2_CALIB);
            connection.stopNotification("no2 raw", GAS_SERVICE, GAS_NO2_RAW);
        }
    }
}
