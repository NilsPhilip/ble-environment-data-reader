/* global index.html : deviceList */

/* global index.css : highlight */

var devicelist = {
    
    selected : false,
    
    size : 0,
    
    initialize : function() {
        devicelist.clear();
        devicelist.lock();
        devicelist.unlock();
    },
    
    lock : function() {
        $("#connection-list-body").off();
        $("#connection-list-head").html("Device List (locked):");
    },
    
    unlock : function() {
        $("#connection-list-body").on(
            "click",
            "li",
            function() {
                if($(this).hasClass("highlight")) {
                    // deselection of current list element
                    // connection not possible anymore
                    devicelist.selected = false;
                    $("#connection-list-body li").removeClass("highlight");
                    connection.currentDevice = [];
                    app.react("device unselect");
                    view.log("device unselect " + this.dataset.deviceId);
                } else {
                    // selection of a list element
                    // connection is now possible
                    devicelist.selected = true;
                    $("#connection-list-body li").removeClass("highlight");
                    $(this).addClass("highlight");
                    connection.currentDevice = this.dataset;
                    app.react("device select");
                    view.log("device select " + this.dataset.deviceId);
                }
            }
        );
        $("#connection-list-head").html("Device List:");
    },
    
    hasSelected : function() {
        return devicelist.selected;
    },
    
    add : function(item) {
        if (devicelist.isEmpty()) {
            $("#connection-list-body").html("");
        }
        $("#connection-list-body").append(item);
        devicelist.size++;
    },
    
    clear : function() {
        $("#connection-list-body").html("List Empty");
        devicelist.size = 0;
        devicelist.selected = false;
    },
    
    isEmpty : function() {
        return devicelist.size == 0;
    },
    
    height : function(newHeight) {
        $("#connection-list").height(newHeight);
        $("#connection-list-body").height(newHeight);
    }
}

var measurementlist = {
    
    size : 0,
    
    initialize : function() {
        measurementlist.clear();
        $("#measurement-list-body").on(
            "click",
            "li",
            function() {
                if($(this).hasClass("highlight")) {
                    $(this).removeClass("highlight");
                    buttons.selectAllMeasurementBtn.setName("select all");
                    view.log("measurement unselected");
                } else {
                    $(this).addClass("highlight");
                    if(measurementlist.getSelected().length == measurementlist.size) {
                        buttons.selectAllMeasurementBtn.setName("deselect all");
                    }
                    view.log("measurement selected");
                }
            }
        );
    },
    
    getSelected : function() {
        var selected = [];
        var i = 0;
        $("#measurement-list-body").children().each(
            function(index)
                {
                    if($(this).hasClass("highlight")) {
                        selected[i++] = this;
                    }
                }
        );
        return selected;
    },
    
    getAll : function() {
        var measurements = [];
        var i = 0;
        $("#measurement-list-body").children().each(
            function(index)
                {
                    measurements[i++] = this;
                }
        );
        return measurements;
    },
    
    selectAll : function() {
        $("#measurement-list-body").children().each(
            function(index)
                {
                    $(this).addClass("highlight");
                }
        );
        buttons.selectAllMeasurementBtn.setName("deselect all");
    },
    
    deselectAll : function() {
        $("#measurement-list-body").children().each(
            function(index)
                {
                    $(this).removeClass("highlight");
                }
        );
        buttons.selectAllMeasurementBtn.setName("select all");
    },
    
    hasSelected : function() {
        var hasSelected = false;
        
        $("#measurement-list-body").children().each(
            function(index)
                {
                    if($(this).hasClass("highlight")) {
                        hasSelected = true;
                    }
                }
        );
        return hasSelected;
    },
    
    add : function(item) {
        if (measurementlist.isEmpty()) {
            $("#measurement-list-body").html("");
        }
        $("#measurement-list-body").append(item);
        measurementlist.size++;
        measurementlist.onChange();
    },
    
    remove : function() {
        $("#measurement-list-body").children().each(
            function(index)
                {
                    if($(this).hasClass("highlight")) {
                        $(this).removeClass("highlight");
                        $(this).remove();
                        measurementlist.size--;
                    }
                }
        );
        if (measurementlist.isEmpty()) {
            $("#measurement-list-body").html("List Empty");
        }
        measurementlist.onChange();
    },
    
    allSelected : function() {
        return measurementlist.getSelected().length == measurementlist.size;
    },
    
    onChange : function() {
        if(!measurementlist.isEmpty && measurementlist.allSelected()) {
            buttons.selectAllMeasurementBtn.setName("deselect all");
        } else {
            buttons.selectAllMeasurementBtn.setName("select all");
        }
    },
    
    clear : function() {
        $("#measurement-list-body").html("List Empty");
        measurementlist.size = 0;
        measurementlist.onChange();
    },
    
    isEmpty : function() {
        return measurementlist.size == 0;
    },
    
    height : function(newHeight) {
        $("#measurement-list").height(newHeight);
        $("#measurement-list-body").height(newHeight);
    }
}